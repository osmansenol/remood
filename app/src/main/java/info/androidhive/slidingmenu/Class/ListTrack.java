package info.androidhive.slidingmenu.Class;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Osman Şenol on 11.05.2015.
 */
public class ListTrack {

    protected int id;
    @SerializedName("trackname")
    protected String name;
    protected String singer;

    public ListTrack(int id, String name, String singer) {
        this.id = id;
        this.name = name;
        this.singer = singer;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getSinger() {
        return this.singer;
    }
}
