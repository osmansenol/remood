package info.androidhive.slidingmenu.Class;

import com.squareup.okhttp.Credentials;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;

/**
 * Created by cuneyt on 11.05.2015.
 */
public class Statics {
    public static final String SERVER_URL_LIST = "http://178.62.235.143/remood/api/v1.0/list";
    public static final String SERVER_URL_QUEUE = "http://178.62.235.143/remood/api/v1.0/queue";
    public static final String SERVER_URL_PLAYING = "http://178.62.235.143/remood/api/v1.0/playing";

    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");
    public static final OkHttpClient client = new OkHttpClient();

    /**
     * Makes post request with OkHttp
     *
     * @param url
     * @param json
     * @return
     * @throws IOException
     */
    public static String post(String url, String json) throws IOException {
        client.setAuthenticator(new com.squareup.okhttp.Authenticator() {
            @Override
            public Request authenticate(java.net.Proxy proxy, Response response) throws IOException {
                String credential = Credentials.basic("umut", "toker");
                return response.request().newBuilder().header("Authorization", credential).build();
            }

            @Override
            public Request authenticateProxy(java.net.Proxy proxy, Response response) throws IOException {
                return null;
            }
        });
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }
}
