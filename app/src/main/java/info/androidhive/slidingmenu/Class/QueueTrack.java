package info.androidhive.slidingmenu.Class;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Osman Şenol on 11.05.2015.
 */
public class QueueTrack extends ListTrack {

    private int user_id;
    @SerializedName("count")
    private int counter;
    @SerializedName("picurl")
    private String user_pic_url;
    @SerializedName("username")
    private String user_name;

    public QueueTrack(int id, String name, String singer, String user_name, String user_pic_url, int counter) {
        super(id, name, singer);
        this.user_pic_url = user_pic_url;
        this.user_name = user_name;
        this.counter = counter;
    }

    public QueueTrack(int id, String name, String singer, int user_id, String user_name, String user_pic_url, int counter) {
        super(id, name, singer);
        this.user_id = user_id;
        this.user_pic_url = user_pic_url;
        this.user_name = user_name;
        this.counter = counter;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public String getUserPicUrl() {
        return this.user_pic_url;
    }

    public String getUserName() {
        return this.user_name;
    }

    public int getUserId() {
        return this.user_id;
    }

}
