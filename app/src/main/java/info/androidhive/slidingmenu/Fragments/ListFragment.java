package info.androidhive.slidingmenu.Fragments;


import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;

import info.androidhive.slidingmenu.Adapter.TrackListAdapter;
import info.androidhive.slidingmenu.Class.ListTemp;
import info.androidhive.slidingmenu.Class.ListTrack;
import info.androidhive.slidingmenu.Class.QueueTrack;
import info.androidhive.slidingmenu.Class.Statics;
import info.androidhive.slidingmenu.Gif.LoadingFragment;
import info.androidhive.slidingmenu.R;
import info.androidhive.slidingmenu.Tasks.PostVoteTask;

public class ListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    ListView track_listview;
    TrackListAdapter trackListAdapter;
    ArrayList<ListTrack> listTracks;
    private SwipeRefreshLayout mSwipeLayout;
    ArrayList<QueueTrack> queueTracks;
    FragmentManager fragmentManager;


    public ListFragment() {
    }

    public ListFragment(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.home_first_item, container, false);

        track_listview = (ListView) rootView.findViewById(R.id.track_listview);

        listTracks = new ArrayList<ListTrack>();


        track_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                new PostVoteTask(new PostVoteTask.PostVoteTaskListener() {
                    @Override
                    public void OnPostSuccess() {

                        Toast.makeText(getActivity(), "OnPostSuccess", Toast.LENGTH_LONG).show();
                        TakeListFromService serviceList = new TakeListFromService();
                        serviceList.execute();
                    }

                    @Override
                    public void OnPostFail(String error) {
                        Toast.makeText(getActivity(), "OnPostFail", Toast.LENGTH_LONG).show();

                    }
                }).execute(makeJson(listTracks, position), Statics.SERVER_URL_QUEUE);
            }
        });

        mSwipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        mSwipeLayout.setOnRefreshListener(this);
        mSwipeLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        listTracks = new ArrayList<ListTrack>();

        TakeListFromService serviceList = new TakeListFromService();
        serviceList.execute();


        return rootView;
    }

    private String makeJson(ArrayList<ListTrack> listTracks, int pos) {
        JSONObject temp = new JSONObject();
        try {
            temp.put("id", listTracks.get(pos).getId());
            temp.put("singer", "" + listTracks.get(pos).getSinger());
            temp.put("trackname", "" + listTracks.get(pos).getName());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("makeJson", "" + temp.toString());
        return temp.toString();
    }

    @Override
    public void onRefresh() {
        TakeListFromService serviceList = new TakeListFromService();
        serviceList.execute();
        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mSwipeLayout.setRefreshing(false);
            }
        }, 5000);*/
    }

    private class TakeListFromService extends AsyncTask<Void, Void, String> {

        private static final String TAG = "PostFetcher";
        ListTemp posts = new ListTemp();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoadingGif();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                //Create an HTTP client
                HttpClient client = new DefaultHttpClient();
                HttpGet get = new HttpGet(Statics.SERVER_URL_LIST);

                get.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials("umut", "toker"), "UTF-8", false));
                //Perform the request and check the status code
                HttpResponse response = client.execute(get);
                StatusLine statusLine = response.getStatusLine();

                if (statusLine.getStatusCode() == 200) {
                    HttpEntity entity = response.getEntity();
                    InputStream content = entity.getContent();

                    try {
                        //Read the server response and attempt to parse it as JSON
                        Reader reader = new InputStreamReader(content);
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        gsonBuilder.setDateFormat("M/d/yy hh:mm a");
                        Gson gson = gsonBuilder.create();

                        posts = gson.fromJson(reader, ListTemp.class);
                        content.close();
                    } catch (Exception ex) {
                        Log.e(TAG, "Failed to parse JSON due to: " + ex);
                        failedLoadingPosts();
                    }
                } else {
                    Log.e(TAG, "Server responded with status code: " + statusLine.getStatusCode());
                    failedLoadingPosts();
                }
            } catch (Exception ex) {
                Log.e(TAG, "Failed to send HTTP POST request due to: " + ex);
                failedLoadingPosts();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            dismissLoadingGif();
            mSwipeLayout.setRefreshing(false);
            if (posts != null) {
                handlePostsList(posts);
            }


        }
    }

    private void handlePostsList(ListTemp posts) {
        this.listTracks = posts.list;

        trackListAdapter = new TrackListAdapter(getActivity(), listTracks);
        track_listview.setAdapter(trackListAdapter);
        trackListAdapter.notifyDataSetChanged();
    }

    private void failedLoadingPosts() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(), "Failed to load Posts. Have a look at LogCat.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public FragmentManager showLoadingGif() {

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.add(R.id.frame_container, LoadingFragment.newInstance("", LoadingFragment.SWING), "Loading");

        fragmentTransaction.commitAllowingStateLoss();

        return fragmentManager;


    }

    public void dismissLoadingGif() {
        try {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    fragmentManager.beginTransaction().remove(fragmentManager.findFragmentByTag("Loading")).commit();
                    fragmentManager.popBackStack();

                }
            }, 600);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
