package info.androidhive.slidingmenu.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import info.androidhive.slidingmenu.Adapter.SamplePagerAdapter;
import info.androidhive.slidingmenu.Model.SlidingTabLayout;
import info.androidhive.slidingmenu.R;

public class PagerFragment extends Fragment {


    FragmentManager fragmentManager;
    SlidingTabLayout tabs;
    private SwipeRefreshLayout mSwipeLayout;

    public PagerFragment(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    public PagerFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.pager_layout, container, false);

        final ViewPager switcher = (ViewPager) rootView.findViewById(R.id.view_switcher);

        SamplePagerAdapter adapter = new SamplePagerAdapter(getActivity(), fragmentManager, new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Toast.makeText(getActivity(), "onRefresh", Toast.LENGTH_LONG).show();

            }
        });

        switcher.setAdapter(adapter);
        switcher.setOffscreenPageLimit(2);

        tabs = (SlidingTabLayout) rootView.findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true);

        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.theme_color);
            }
        });

        tabs.setViewPager(switcher);


        return rootView;
    }
}