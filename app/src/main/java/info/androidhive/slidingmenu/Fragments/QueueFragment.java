package info.androidhive.slidingmenu.Fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;

import info.androidhive.slidingmenu.Adapter.QueueListAdapter;
import info.androidhive.slidingmenu.Class.PlayingTemp;
import info.androidhive.slidingmenu.Class.QueueTemp;
import info.androidhive.slidingmenu.Class.QueueTrack;
import info.androidhive.slidingmenu.Class.Statics;
import info.androidhive.slidingmenu.Gif.LoadingFragment;
import info.androidhive.slidingmenu.R;

public class QueueFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    ListView queue_listview;
    QueueListAdapter queueListAdapter;
    ArrayList<QueueTrack> queueTracks;

    FragmentManager fragmentManager;
    private SwipeRefreshLayout mSwipeLayout;

    ListView playing_listview;
    ArrayList<QueueTrack> playingTrack;
    QueueListAdapter playingListAdapter;
    private boolean forQueue = false;

    public QueueFragment() {
    }

    public QueueFragment(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.home_second_item, container, false);

        queue_listview = (ListView) rootView.findViewById(R.id.track_listview);
        playing_listview = (ListView) rootView.findViewById(R.id.playing_listview);

        mSwipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        mSwipeLayout.setOnRefreshListener(this);
        mSwipeLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        queueTracks = new ArrayList<QueueTrack>();
        playingTrack = new ArrayList<QueueTrack>();

        TakePlayingFromService serviceList2 = new TakePlayingFromService();
        serviceList2.execute();

        TakeQueueFromService serviceList = new TakeQueueFromService();
        serviceList.execute();


        return rootView;
    }

    @Override
    public void onRefresh() {
        TakePlayingFromService serviceList2 = new TakePlayingFromService();
        serviceList2.execute();
        TakeQueueFromService serviceList = new TakeQueueFromService();
        serviceList.execute();

    }

    private class TakeQueueFromService extends AsyncTask<Void, Void, String> {
        private static final String TAG = "PostFetcher";

        QueueTemp posts = new QueueTemp();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoadingGif();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                //Create an HTTP client
                HttpClient client = new DefaultHttpClient();
                //HttpPost post = new HttpPost(SERVER_URL);
                HttpGet get = new HttpGet(Statics.SERVER_URL_QUEUE);

                get.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials("umut", "toker"), "UTF-8", false));

                //Perform the request and check the status code
                HttpResponse response = client.execute(get);
                StatusLine statusLine = response.getStatusLine();

                if (statusLine.getStatusCode() == 200) {
                    HttpEntity entity = response.getEntity();
                    InputStream content = entity.getContent();

                    try {
                        //Read the server response and attempt to parse it as JSON
                        Reader reader = new InputStreamReader(content);
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        gsonBuilder.setDateFormat("M/d/yy hh:mm a");
                        Gson gson = gsonBuilder.create();
                        posts = gson.fromJson(reader, QueueTemp.class);
                        content.close();

                    } catch (Exception ex) {
                        Log.e(TAG, "Failed to parse JSON due to: " + ex);
                        failedLoadingPosts();
                    }
                } else {
                    Log.e(TAG, "Server responded with status code: " + statusLine.getStatusCode());
                    failedLoadingPosts();
                }
            } catch (Exception ex) {
                Log.e(TAG, "Failed to send HTTP POST request due to: " + ex);
                failedLoadingPosts();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            dismissLoadingGif();
            mSwipeLayout.setRefreshing(false);

            if (posts.queue != null) {
                handlePostsQueue(posts);
            }
        }
    }

    private class TakePlayingFromService extends AsyncTask<Void, Void, String> {

        private static final String TAG = "PostFetcher";

        PlayingTemp posts = new PlayingTemp();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showLoadingGif();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                //Create an HTTP client
                HttpClient client = new DefaultHttpClient();
                //HttpPost post = new HttpPost(SERVER_URL);
                HttpGet get = new HttpGet(Statics.SERVER_URL_PLAYING);

                get.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials("umut", "toker"), "UTF-8", false));

                //Perform the request and check the status code
                HttpResponse response = client.execute(get);
                StatusLine statusLine = response.getStatusLine();

                if (statusLine.getStatusCode() == 200) {
                    HttpEntity entity = response.getEntity();
                    InputStream content = entity.getContent();

                    try {
                        //Read the server response and attempt to parse it as JSON
                        Reader reader = new InputStreamReader(content);
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        gsonBuilder.setDateFormat("M/d/yy hh:mm a");
                        Gson gson = gsonBuilder.create();
                        posts = gson.fromJson(reader, PlayingTemp.class);
                        content.close();

                    } catch (Exception ex) {
                        Log.e(TAG, "Failed to parse JSON due to: " + ex);
                        failedLoadingPosts();
                    }
                } else {
                    Log.e(TAG, "Server responded with status code: " + statusLine.getStatusCode());
                    failedLoadingPosts();
                }
            } catch (Exception ex) {
                Log.e(TAG, "Failed to send HTTP POST request due to: " + ex);
                failedLoadingPosts();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            // dismissLoadingGif();
            mSwipeLayout.setRefreshing(false);

            if (posts.playing != null) {
                handlePostsPlaying(posts);
            }
        }
    }

    public FragmentManager showLoadingGif() {

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.add(R.id.frame_container, LoadingFragment.newInstance("", LoadingFragment.SWING), "Loading");

        fragmentTransaction.commitAllowingStateLoss();

        return fragmentManager;


    }

    private void handlePostsQueue(QueueTemp posts) {
//        Log.d("handlePostsQueue",""+posts.queue.get(0).getSinger());
        this.queueTracks = posts.queue;

        queueListAdapter = new QueueListAdapter(getActivity(), queueTracks, !forQueue);
        queue_listview.setAdapter(queueListAdapter);
        queueListAdapter.notifyDataSetChanged();
    }

    private void handlePostsPlaying(PlayingTemp posts) {
        //Log.d("handlePostsPlaying",""+posts.playing.get(0).getSinger());
        this.playingTrack = posts.playing;

        playingListAdapter = new QueueListAdapter(getActivity(), playingTrack, forQueue);
        playing_listview.setAdapter(playingListAdapter);
        playingListAdapter.notifyDataSetChanged();
    }

    private void failedLoadingPosts() {
        (getActivity()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(), "Failed to load Posts. Have a look at LogCat.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void dismissLoadingGif() {
        try {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    fragmentManager.beginTransaction().remove(fragmentManager.findFragmentByTag("Loading")).commit();
                    fragmentManager.popBackStack();

                }
            }, 600);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
