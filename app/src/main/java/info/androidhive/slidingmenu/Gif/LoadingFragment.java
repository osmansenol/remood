package info.androidhive.slidingmenu.Gif;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import info.androidhive.slidingmenu.R;

public class LoadingFragment extends BaseFragment {

    public static final int SWING = 0;
    public static final int PLUS = 1;

    private static final String TEXT = "text";


    TextView tvExtra;

    GifMovieView gif1;

    GifMovieView gif2;


    private String extraText;
    private int mode = 0;


    public static LoadingFragment newInstance(String sectionNumber, int type) {
        LoadingFragment fragment = new LoadingFragment();
        Bundle args = new Bundle();
        args.putString(TEXT, sectionNumber);
        args.putInt("TYPE", type);
        fragment.setArguments(args);
        return fragment;
    }

    public LoadingFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            extraText = getArguments().getString(TEXT);
            mode = getArguments().getInt("TYPE", 0);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_loading, container, false);

        tvExtra = (TextView) view.findViewById(R.id.textViewLoading);
        gif1 = (GifMovieView) view.findViewById(R.id.gif_1);

        //RelativeLayout.LayoutParams layoutParams;

        //layoutParams = new RelativeLayout.LayoutParams((int) (StaticData.width * ((float) 1 / 3)),
        //(int) (StaticData.width * ((float) 1 / 3)));
        // gif1.setLayoutParams(layoutParams);
        //s gif2.setLayoutParams(layoutParams);

        if (extraText != null) {
            tvExtra.setVisibility(View.VISIBLE);
            tvExtra.setText(extraText);
        }

        if (mode < 1) {
            view.findViewById(R.id.gif_1).setVisibility(View.VISIBLE);
        } else {
            view.findViewById(R.id.gif_1).setVisibility(View.VISIBLE);
        }

        return view;
    }
}