package info.androidhive.slidingmenu.Tasks;

import android.os.AsyncTask;

import org.json.JSONObject;

import info.androidhive.slidingmenu.Class.Statics;
import info.androidhive.slidingmenu.Service.ServicesResponse;

/**
 * Created by cuneyt on 12.05.2015.
 */
public class PostVoteTask extends AsyncTask<String, Void, String> {

    public interface PostVoteTaskListener {
        public void OnPostSuccess();

        public void OnPostFail(String error);
    }

    private PostVoteTaskListener listener;

    public PostVoteTask(PostVoteTaskListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {

        try {

            String req = params[0];
            String response = Statics.post(params[1], req);

            return response;
        } catch (Exception e) {

            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        ServicesResponse.parseResponse(s, new ServicesResponse.GetListListener() {

            @Override
            public void onResponseSuccess(int status, JSONObject track) {
                listener.OnPostSuccess();
            }

            @Override
            public void onResponseFailed(int status, String error) {
                listener.OnPostFail(error);
            }
        });


    }


}
