package info.androidhive.slidingmenu.Adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import info.androidhive.slidingmenu.Class.ListTrack;
import info.androidhive.slidingmenu.R;

/**
 * Created by Osman Şenol on 11.05.2015.
 */
public class TrackListAdapter extends BaseAdapter implements AdapterView.OnItemClickListener {

    private Context context;
    ListTrack listTrack;
    private ArrayList<ListTrack> listTracks;
    private static LayoutInflater inflater = null;

    public static class ViewHolder {

        public TextView list_track_name;
        public TextView list_track_singer;

    }

    public TrackListAdapter(Context context, ArrayList<ListTrack> listTracks) {
        this.listTracks = listTracks;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listTracks.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;

        if (convertView == null) {
            view = inflater.inflate(R.layout.track_list, null);

            holder = new ViewHolder();
            holder.list_track_name = (TextView) view.findViewById(R.id.list_track_name);
            holder.list_track_singer = (TextView) view.findViewById(R.id.list_track_singer);

            view.setTag(holder);
        } else
            holder = (ViewHolder) view.getTag();

        if (listTracks.size() <= 0) {
            holder.list_track_name.setText("No Data");
            holder.list_track_singer.setText("No Data");
        } else {

            listTrack = null;
            listTrack = listTracks.get(position);

            holder.list_track_name.setText(listTrack.getName());
            holder.list_track_singer.setText(listTrack.getSinger());
        }

        return view;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Context context = view.getContext();

        Toast.makeText(context, "name : " + listTracks.get(position).getName() +
                "\n singer : " + listTracks.get(position).getSinger(), Toast.LENGTH_SHORT).show();
    }

}
