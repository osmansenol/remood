package info.androidhive.slidingmenu.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.widget.SwipeRefreshLayout;

import info.androidhive.slidingmenu.Fragments.ListFragment;
import info.androidhive.slidingmenu.Fragments.QueueFragment;

/**
 * Created by osman on 11.05.2015.
 */
public class SamplePagerAdapter extends FragmentStatePagerAdapter {

    Context context;
    FragmentManager fragmentManager;
    SwipeRefreshLayout.OnRefreshListener refreshListener;

    public SamplePagerAdapter(Context context, FragmentManager fragmentManager, SwipeRefreshLayout.OnRefreshListener refreshListener) {
        super(fragmentManager);

        this.context = context;
        this.fragmentManager = fragmentManager;
        this.refreshListener = refreshListener;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) // if the position is 0 we are returning the First tab
        {
            ListFragment tab1 = new ListFragment(fragmentManager);
            return tab1;
        } else             // As we are having 2 tabs if the position is now 0 it must be 1 so we are returning second tab
        {
            QueueFragment tab2 = new QueueFragment(fragmentManager);
            return tab2;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0)
            return "Play List";
        else
            return "Queue";
    }
}
