package info.androidhive.slidingmenu.Adapter;


import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import info.androidhive.slidingmenu.Class.QueueTrack;
import info.androidhive.slidingmenu.Class.Statics;
import info.androidhive.slidingmenu.Gif.GifMovieView;
import info.androidhive.slidingmenu.R;
import info.androidhive.slidingmenu.Tasks.PostVoteTask;

/**
 * Created by Osman Şenol on 11.05.2015.
 */
public class QueueListAdapter extends BaseAdapter {

    public static ImageLoader imageLoader = ImageLoader.getInstance();

    public static void InitImageLoader(Context context) {

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_stumb)
                .showImageForEmptyUri(R.drawable.ic_empty)
                .showImageOnFail(R.drawable.ic_error)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(options)
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
    }


    private Context context;
    QueueTrack queueTrack;
    private ArrayList<QueueTrack> queueTracks;
    private static LayoutInflater inflater = null;
    private boolean forQueue;

    public static class ViewHolder {
        public TextView queue_track_name;
        public TextView queue_track_singer;
        public TextView queue_track_user_name;
        public TextView queue_track_counter;
        public ImageView queue_track_vote;
        public ImageView queue_track_user_image;
        public GifMovieView queue_track_gif;
    }

    public QueueListAdapter(Context context, ArrayList<QueueTrack> queueTracks, boolean forQueue) {
        InitImageLoader(context);
        this.queueTracks = queueTracks;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.forQueue = forQueue;
    }

    @Override
    public int getCount() {
        return queueTracks.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            if (forQueue) {
                view = inflater.inflate(R.layout.queue_list, null);
                holder.queue_track_vote = (ImageView) view.findViewById(R.id.queue_track_vote);

            } else {

                view = inflater.inflate(R.layout.playing_list_item, null);
                holder.queue_track_vote = (ImageView) view.findViewById(R.id.queue_track_vote);
                holder.queue_track_gif = (GifMovieView) view.findViewById(R.id.gif_1);
                holder.queue_track_gif.setVisibility(View.VISIBLE);

            }
            holder.queue_track_name = (TextView) view.findViewById(R.id.queue_track_name);
            holder.queue_track_singer = (TextView) view.findViewById(R.id.queue_track_singer);
            holder.queue_track_user_name = (TextView) view.findViewById(R.id.queue_track_user_name);
            holder.queue_track_counter = (TextView) view.findViewById(R.id.queue_track_counter);
            holder.queue_track_user_image = (ImageView) view.findViewById(R.id.queue_track_user_image);
            holder.queue_track_vote = (ImageView) view.findViewById(R.id.queue_track_vote);


            view.setTag(holder);
        } else
            holder = (ViewHolder) view.getTag();

        if (queueTracks.size() <= 0) {
            holder.queue_track_name.setText("No Data");
            holder.queue_track_singer.setText("No Data");
            holder.queue_track_user_name.setText("No Data");
            holder.queue_track_counter.setText("0");
        } else {

            queueTrack = null;
            queueTrack = queueTracks.get(position);

            holder.queue_track_name.setText(queueTrack.getName());
            holder.queue_track_singer.setText(queueTrack.getSinger());
            holder.queue_track_user_name.setText(queueTrack.getUserName());
            if (forQueue)
                holder.queue_track_counter.setText("+" + queueTrack.getCounter());
            // Log.d("url : ", queueTrack.getUserPicUrl());
            imageLoader.displayImage(queueTrack.getUserPicUrl(), holder.queue_track_user_image);
            holder.queue_track_vote.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    new PostVoteTask(new PostVoteTask.PostVoteTaskListener() {
                        @Override
                        public void OnPostSuccess() {

                            Toast.makeText(context, "OnPostSuccess", Toast.LENGTH_LONG).show();
                            holder.queue_track_vote.setImageResource(R.drawable.ic_chevron_up_voted);
                            holder.queue_track_counter.setText("+" + (Integer.parseInt(holder.queue_track_counter.getText().toString().substring(1)) + 1));

                        }

                        @Override
                        public void OnPostFail(String error) {
                            Toast.makeText(context, "OnPostFail", Toast.LENGTH_LONG).show();

                        }
                    }).execute(makeJson(queueTracks, position), Statics.SERVER_URL_QUEUE);


                }
            });
        }

        return view;
    }

    private String makeJson(ArrayList<QueueTrack> queueTracks, int pos) {
        JSONObject temp = new JSONObject();
        try {
            temp.put("id", queueTracks.get(pos).getId());
            temp.put("singer", "" + queueTracks.get(pos).getSinger());
            temp.put("trackname", "" + queueTracks.get(pos).getName());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("makeJson", "" + temp.toString());
        return temp.toString();
    }


}
