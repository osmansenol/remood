package info.androidhive.slidingmenu.Service;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by cuneyt on 22.01.2015.
 */
public class ServicesResponse {


    //For registiration
    public interface GetListListener {
        public void onResponseSuccess(int status, JSONObject list);

        public void onResponseFailed(int status, String error);
    }


    public static void parseResponse(String data, GetListListener listener) {

        JSONObject jsonObject;
        JSONObject resultJson;
        int status = -1;
        String error = "";
        Log.d("data", "" + data);

        try {
            jsonObject = new JSONObject(data);
            //  if (!jsonObject.isNull("track"))
            {
                resultJson = jsonObject.getJSONObject("track");
                listener.onResponseSuccess(status, resultJson);
            }
            //  else
            //    listener.onResponseFailed(status, error);
        } catch (JSONException j) {
            j.printStackTrace();
            listener.onResponseFailed(status, error);
        }


    }

}
